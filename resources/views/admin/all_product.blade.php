@extends('admin_layout')
@section('admin_content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liệt Kê Sản Phẩm
            </div>
            <center>
                <?php
                $message = \Illuminate\Support\Facades\Session::get('message');
                if ($message) {
                    echo '<span style="text-align: center;font-size: 17px;color: red;font-weight: bold;width: 100%;" ">' . $message . '</span>';
                    \Illuminate\Support\Facades\Session::put('message', null);
                }
                ?>
            </center>
            <div class="row w3-res-tb">
                <div class="col-sm-5 m-b-xs">
                    <select class="input-sm form-control w-sm inline v-middle">
                        <option value="0">Bulk action</option>
                        <option value="1">Delete selected</option>
                        <option value="2">Bulk edit</option>
                        <option value="3">Export</option>
                    </select>
                    <button class="btn btn-sm btn-default">Apply</button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="text" class="input-sm form-control" placeholder="Search">
                        <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Tìm!</button>
          </span>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>Tên sản phẩm</th>
                        <th>Gía</th>
                        <th>Hình ảnh</th>
                        <th>Danh mục</th>
                        <th>Thương hiệu</th>
                        <th>Hiển thị</th>
                        <th style="width:30px;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($all_product as $key => $Product)
                        <tr>
                            <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label>
                            </td>
                            <td>{{ $Product->product_name }}</td>
                            <td>{{ $Product->product_price }}</td>
                            <td><img src="public/uploads/product/{{ $Product->product_image }}" width="50" height="50">
                            </td>
                            <td>{{ $Product->category_name }}</td>
                            <td>{{ $Product->brand_name }}</td>
                            <td><span class="text-ellipsis">
                                    @if($Product->product_status == 0)
                                        <a href="{{URL::to('/active-product/'.$Product->product_id)}}"><span
                                                class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    @else
                                        <a href="{{URL::to('/unactive-product/'.$Product->product_id)}}"><span
                                                class="fa-thumb-styling fa fa-thumbs-up"></span></a>
                                    @endif
                                </span>
                            </td>
                            <td>
                                <a href="{{URL::to('/edit-product/'.$Product->product_id)}}" class="active"
                                   ui-toggle-class="">
                                    <i class="styling-edit fa fa-pencil-square-o text-success text-active"></i>
                                </a>
                                <a onclick="return confirm('Bạn có chắc muốn xóa sản phẩm này không?')"
                                   href="{{URL::to('/delete-product/'.$Product->product_id)}}" class="active"
                                   ui-toggle-class="">
                                    <i class="styling-edit fa fa-times text-danger text"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">

                    <div class="col-sm-5 text-center">
                        <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
                    </div>
                    <div class="col-sm-7 text-right text-center-xs">
                        <ul class="pagination pagination-sm m-t-none m-b-none">
                            <li><a href=""><i class="fa fa-chevron-left"></i></a></li>
                            <li><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href=""><i class="fa fa-chevron-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
@endsection
