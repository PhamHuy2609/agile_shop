@extends('admin_layout')
@section('admin_content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Cập nhật sản sản phẩm
                </header>
                <center>
                    <?php
                    $message = \Illuminate\Support\Facades\Session::get('message');
                    if ($message) {
                        echo '<span style="text-align: center;font-size: 17px;color: red;font-weight: bold;width: 100%;" ">' . $message . '</span>';
                        \Illuminate\Support\Facades\Session::put('message', null);
                    }
                    ?>
                </center>
                <div class="panel-body">
                    <div class="position-center">
                        @foreach($edit_products as $key => $edit_product)
                            <form role="form" action="{{URL::to('/update-product/'.$edit_product->product_id)}}"
                                  method="post"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên sản phẩm</label>
                                    <input type="text" name="product_name" class="form-control" id="exampleInputEmail1"
                                           value="{{$edit_product->product_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                                    <select name="product_category" class="form-control input-sm m-bot15">
                                        @foreach($categoryProducts as $key => $categoryProduct)
                                            @if($categoryProduct->category_id == $edit_product->category_id)
                                                <option selected
                                                        value="{{$categoryProduct->category_id}}">{{$categoryProduct->category_name}}</option>
                                            @else
                                                <option
                                                    value="{{$categoryProduct->category_id}}">{{$categoryProduct->category_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Thương hiệu sản phẩm</label>
                                    <select name="product_brand" class="form-control input-sm m-bot15">
                                        @foreach($brandProducts as $key => $brandProduct)
                                            @if($brandProduct->brand_id == $edit_product->brand_id)
                                                <option selected
                                                        value="{{$brandProduct->brand_id}}">{{$brandProduct->brand_name}}</option>
                                            @else
                                                <option
                                                    value="{{$brandProduct->brand_id}}">{{$brandProduct->brand_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Gía sản phẩm</label>
                                    <input type="text" name="product_price" class="form-control" id="exampleInputEmail1"
                                           value="{{$edit_product->product_price}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hình ảnh sản phẩm</label>
                                    <input type="file" name="product_image" class="form-control"
                                           id="exampleInputEmail1">
                                    <img style="padding-top: 5px;"
                                         src="{{URL::to('public/uploads/product/'.$edit_product->product_image)}}"
                                         width="150" height="150" alt="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Mô tả sản phẩm</label>
                                    <textarea style="resize: none;" rows="5" class="form-control"
                                              name="product_desc">{{$edit_product->product_desc}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Nội dung sản phẩm</label>
                                    <textarea style="resize: none;" rows="5" class="form-control"
                                              name="product_content">{{$edit_product->product_content}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Hiển thị</label>
                                    <select name="product_status" class="form-control input-sm m-bot15">
                                            @if($edit_product->product_status == 0)
                                                <option value="0">Ẩn</option>
                                            @else
                                            <option value="1">Hiện</option>
                                            @endif
                                        </option>
                                    </select>
                                </div>
                                <button type="submit" name="add_product" class="btn btn-info">Cập nhật sản phẩm</button>
                            </form>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
