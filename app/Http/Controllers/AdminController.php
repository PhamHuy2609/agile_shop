<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

session_start();

class AdminController extends Controller
{

    public function index()
    {
        return view('admin_login');
    }

    public function show_dashboard()
    {
        $this->Authlogin();
        return view('admin.dashboard');
    }

    public function dashboard(Request $request)
    {
        $admin_email = $request->admin_email;
        $admin_password = $request->admin_password;
        $results = DB::table('tbl_admin')
                ->where('admin_email', $admin_email)
                ->where('admin_password', $admin_password)
                ->first();

        if(!empty($results)) {
            Session::put('admin_name', $results->admin_name);
            Session::put('admin_id', $results->admin_id);
            return Redirect::to('dashboard');
        } else {
            Session::put('message', 'Mật khẩu hoặc tài khoản không đúng, vui lòng nhập lại');
            return Redirect::to('/admin');
        }
    }

    public function logout()
    {
        $this->Authlogin();
        Session::put('admin_name', null);
        Session::put('admin_id', null);
        return Redirect::to('/admin');
    }

}
