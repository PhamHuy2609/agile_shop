<?php

namespace App\Http\Controllers;

use App\TblCategoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();

class CategoryProductController extends Controller
{
    // Admin *****************************************************
    public function all_category_product()
    {
        self::Authlogin();
        $all_category_product = TblCategoryProduct::all();
//        $all_category_product = DB::table('tbl_category_product')->get();
        $manager_category_product = view('admin.all_category_product')->with('all_category_product', $all_category_product);
        return view('admin_layout')->with('admin.all_category_product', $manager_category_product);
    }

    public function add_category_product()
    {
        self::Authlogin();
        return view('admin.add_category_product');
    }

    public function save_category_product(Request $request)
    {
        self::Authlogin();
        $data = array();
        $data['category_name'] = $request->category_product_name;
        $data['category_desc'] = $request->category_product_desc;
        $data['category_status'] = $request->category_product_status;
        DB::table('tbl_category_product')->insert($data);
        Session::put('message', 'Thêm danh mục sản phẩm thành công');
        return Redirect::to('add-category-product');
    }

    public function unactive_category_product($category_id)
    {
        self::Authlogin();
        DB::table('tbl_category_product')->where('category_id', $category_id)->update(['category_status'=>0]);
        return Redirect::to('all-category-product');
    }

    public function active_category_product($category_id)
    {
        self::Authlogin();
        DB::table('tbl_category_product')->where('category_id', $category_id)->update(['category_status'=>1]);
        return Redirect::to('all-category-product');
    }

    public function edit_category_product($category_id)
    {
        self::Authlogin();
        $all_category_product = DB::table('tbl_category_product')->where('category_id', $category_id)->get();
        $manager_category_product = view('admin.edit_category_product')->with('edit_category_product', $all_category_product);
        return view('admin_layout')->with('admin.edit_category_product', $manager_category_product);
    }

    public function update_category_product(Request $request, $category_id)
    {
        self::Authlogin();
        $data = array();
        $data['category_name'] = $request->category_product_name;
        $data['category_desc'] = $request->category_product_desc;
        DB::table('tbl_category_product')->where('category_id', $category_id)->update($data);
        Session::put('message', 'Cập nhật danh mục sản phẩm thành công');
        return Redirect::to('all-category-product');
    }

    public function delete_category_product($category_id)
    {
        self::Authlogin();
        DB::table('tbl_category_product')->where('category_id', $category_id)->delete();
        Session::put('message', 'Xóa danh mục sản phẩm thành công');
        return Redirect::to('all-category-product');
    }

    // End Admin *******************************************
    // Home ************************************************
    public function show_category_home($category_id)
    {
        $categoryProducts = DB::table('tbl_category_product')
            ->where('category_status', '1')
            ->orderBy('category_id', 'desc')
            ->get();
        $brandProducts = DB::table('tbl_brand_product')
            ->where('brand_status', '1')
            ->orderBy('brand_id', 'desc')
            ->get();
        $category_by_id = DB::table('tbl_product')
            ->join('tbl_category_product','tbl_category_product.category_id', '=', 'tbl_product.category_id')
            ->where('tbl_product.category_id', $category_id)
            ->get();
        $category_names = DB::table('tbl_category_product')
            ->where('category_id', $category_id)
            ->limit(1)
            ->get();
        return view('papes.category.show_category')->with([
                'categoryProducts' => $categoryProducts,
                'brandProducts' => $brandProducts,
                'product_by_categorys' => $category_by_id,
                'category_names' => $category_names,
            ]);
    }


}
