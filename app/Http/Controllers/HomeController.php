<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $categoryProducts = DB::table('tbl_category_product')
            ->where('category_status', '1')
            ->orderBy('category_id', 'desc')
            ->get();
        $brandProducts = DB::table('tbl_brand_product')
            ->where('brand_status', '1')
            ->orderBy('brand_id', 'desc')
            ->get();
        $all_Products = DB::table('tbl_product')
            ->where('product_status', '1')
            ->limit(6)
            ->orderBy('product_id', 'desc')
            ->get();

        return view('papes.home')->with([
            'categoryProducts' => $categoryProducts,
            'brandProducts' => $brandProducts,
            'all_Products' => $all_Products,
        ]);
    }
}
