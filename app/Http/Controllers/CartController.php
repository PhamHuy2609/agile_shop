<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

session_start();

class CartController extends Controller
{
    public function save_cart(Request $request)
    {
        $productId = $request->productId_hidden;
        $quantity = $request->qty;
        $product_info = DB::table('tbl_product')->where('product_id', $productId)->first();

        $data['id'] = $product_info->product_id;
        $data['qty'] = $quantity;
        $data['name'] = $product_info->product_name;
        $data['price'] = $product_info->product_price;
        $data['weight'] = "134";
        $data['options']['image'] = $product_info->product_image;
//        Cart::destroy();
        Cart::add($data);
        return Redirect::to('show-cart');

    }

    public function show_cart()
    {
        $categoryProducts = DB::table('tbl_category_product')->orderBy('category_id', 'desc')->get();
        $brandProducts = DB::table('tbl_brand_product')->orderBy('brand_id', 'desc')->get();

        return view('papes.cart.show_cart')->with([
            'categoryProducts' => $categoryProducts,
            'brandProducts'    => $brandProducts,
        ]);
    }

    public function delete_to_cart($rowId)
    {
        Cart::update($rowId, 0);
        return Redirect::to('show-cart');
    }
}
