<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

session_start();

class ProductController extends Controller
{
    // Admin  ******************************************

    public function all_product()
    {
        self::Authlogin();
        $all_product = DB::table('tbl_product')
            ->join('tbl_category_product', 'tbl_category_product.category_id', '=', 'tbl_product.category_id')
            ->join('tbl_brand_product', 'tbl_brand_product.brand_id', '=', 'tbl_product.brand_id')
            ->orderBy('tbl_product.product_id', 'desc')->get();
        $manager_product = view('admin.all_product')->with('all_product', $all_product);
        return view('admin_layout')->with('admin.all_product', $manager_product);
    }

    public function add_product()
    {
        self::Authlogin();
        $categoryProducts = DB::table('tbl_category_product')->orderBy('category_id', 'desc')->get();
        $brandProducts = DB::table('tbl_brand_product')->orderBy('brand_id', 'desc')->get();
        return view('admin.add_product')->with([
            'categoryProducts' => $categoryProducts,
            'brandProducts'    => $brandProducts,
        ]);
    }

    public function save_product(Request $request)
    {
        self::Authlogin();
        $data = array();
        $data['product_name'] = $request->product_name;
        $data['category_id'] = $request->product_category;
        $data['brand_id'] = $request->product_brand;
        $data['product_price'] = $request->product_price;
        $data['product_desc'] = $request->product_desc;
        $data['product_content'] = $request->product_content;
        $data['product_status'] = $request->product_status;
        $get_image = $request->file('product_image');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.', $get_name_image));
            $new_image = $name_image . '-' . rand(0, 999) . '.' . $get_image->getClientOriginalExtension();
            $get_image->move('public/uploads/product', $new_image);
            $data['product_image'] = $new_image;
            DB::table('tbl_product')->insert($data);
            Session::put('message', 'Thêm sản phẩm thành công');
            return Redirect::to('all-product');

        }
        $data['product_image'] = '';
        DB::table('tbl_product')->insert($data);
        Session::put('message', 'Thêm sản phẩm thành công');
        return Redirect::to('all-product');
    }

    public function unactive_product($product_id)
    {
        self::Authlogin();
        DB::table('tbl_product')->where('product_id', $product_id)->update(['product_status' => 0]);
        return Redirect::to('all-product');
    }

    public function active_product($product_id)
    {
        self::Authlogin();
        DB::table('tbl_product')->where('product_id', $product_id)->update(['product_status' => 1]);
        return Redirect::to('all-product');
    }

    public function edit_product($product_id)
    {
        self::Authlogin();
        $categoryProducts = DB::table('tbl_category_product')->orderBy('category_id', 'desc')->get();
        $brandProducts = DB::table('tbl_brand_product')->orderBy('brand_id', 'desc')->get();
        $all_products = DB::table('tbl_product')->where('product_id', $product_id)->get();
        $manager_product = view('admin.edit_product')
            ->with([
                'edit_products'    => $all_products,
                'categoryProducts' => $categoryProducts,
                'brandProducts'    => $brandProducts,
            ]);
        return view('admin_layout')->with('admin.edit_product', $manager_product);
    }

    public function update_product(Request $request, $product_id)
    {
        self::Authlogin();
        $data = array();
        $data['product_name'] = $request->product_name;
        $data['category_id'] = $request->product_category;
        $data['brand_id'] = $request->product_brand;
        $data['product_price'] = $request->product_price;
        $data['product_desc'] = $request->product_desc;
        $data['product_content'] = $request->product_content;
        $data['product_status'] = $request->product_status;
        $get_image = $request->file('product_image');
        if ($get_image) {
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.', $get_name_image));
            $new_image = $name_image . '-' . rand(0, 999) . '.' . $get_image->getClientOriginalExtension();
            $get_image->move('public/uploads/product', $new_image);
            $data['product_image'] = $new_image;
            DB::table('tbl_product')->where('product_id', $product_id)->update($data);
            Session::put('message', 'Cập nhật sản phẩm thành công');
            return Redirect::to('all-product');

        }
        DB::table('tbl_product')->where('product_id', $product_id)->update($data);
        Session::put('message', 'Cập nhật sản phẩm thành công');
        return Redirect::to('all-product');
    }

    public function delete_product($product_id)
    {
        self::Authlogin();
        DB::table('tbl_product')->where('product_id', $product_id)->delete();
        Session::put('message', 'Xóa sản phẩm thành công');
        return Redirect::to('all-product');
    }

    // End Admin  ******************************************

    // Home/ Details Product
    public function show_detail_product($product_id)
    {
        $categoryProducts = DB::table('tbl_category_product')->orderBy('category_id', 'desc')->get();
        $brandProducts = DB::table('tbl_brand_product')->orderBy('brand_id', 'desc')->get();
        $productDetails = DB::table('tbl_product')
            ->join('tbl_category_product', 'tbl_category_product.category_id', '=', 'tbl_product.category_id')
            ->join('tbl_brand_product', 'tbl_brand_product.brand_id', '=', 'tbl_product.brand_id')
            ->where('tbl_product.product_id', $product_id)->get();
        return view('papes.product.show_product_details')->with([
            'categoryProducts' => $categoryProducts,
            'brandProducts'    => $brandProducts,
            'productDetails'  => $productDetails,
        ]);
    }
}
