<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();

class BrandProductController extends Controller
{
    // Admin **************************************
    public function all_brand_product()
    {
        self::Authlogin();
        $all_brand_product = DB::table('tbl_brand_product')->get();
        $manager_brand_product = view('admin.all_brand_product')->with('all_brand_product', $all_brand_product);
        return view('admin_layout')->with('admin.all_brand_product', $manager_brand_product);
    }

    public function add_brand_product()
    {
        self::Authlogin();
        return view('admin.add_brand_product');
    }

    public function save_brand_product(Request $request)
    {
        self::Authlogin();
        $data = array();
        $data['brand_name'] = $request->brand_product_name;
        $data['brand_desc'] = $request->brand_product_desc;
        $data['brand_status'] = $request->brand_product_status;
        DB::table('tbl_brand_product')->insert($data);
        Session::put('message', 'Thêm thương hiệu sản phẩm thành công');
        return Redirect::to('add-brand-product');
    }

    public function unactive_brand_product($brand_id)
    {
        self::Authlogin();
        DB::table('tbl_brand_product')->where('brand_id', $brand_id)->update(['brand_status'=>0]);
        return Redirect::to('all-brand-product');
    }

    public function active_brand_product($brand_id)
    {
        self::Authlogin();
        DB::table('tbl_brand_product')->where('brand_id', $brand_id)->update(['brand_status'=>1]);
        return Redirect::to('all-brand-product');
    }

    public function edit_brand_product($brand_id)
    {
        self::Authlogin();
        $all_brand_product = DB::table('tbl_brand_product')->where('brand_id', $brand_id)->get();
        $manager_brand_product = view('admin.edit_brand_product')->with('edit_brand_product', $all_brand_product);
        return view('admin_layout')->with('admin.edit_brand_product', $manager_brand_product);
    }

    public function update_brand_product(Request $request, $brand_id)
    {
        self::Authlogin();
        $data = array();
        $data['brand_name'] = $request->brand_product_name;
        $data['brand_desc'] = $request->brand_product_desc;
        DB::table('tbl_brand_product')->where('brand_id', $brand_id)->update($data);
        Session::put('message', 'Cập nhật thương hiẹu sản phẩm thành công');
        return Redirect::to('all-brand-product');
    }

    public function delete_brand_product($brand_id)
    {
        self::Authlogin();
        DB::table('tbl_brand_product')->where('brand_id', $brand_id)->delete();
        Session::put('message', 'Xóa danh thương hiệu sản phẩm thành công');
        return Redirect::to('all-brand-product');
    }
    // End Admin ********************************

    // Home / Brand
    public function show_brand_home($brand_id)
    {
        $categoryProducts = DB::table('tbl_category_product')
            ->where('category_status', '1')
            ->orderBy('category_id', 'desc')
            ->get();
        $brandProducts = DB::table('tbl_brand_product')
            ->where('brand_status', '1')
            ->orderBy('brand_id', 'desc')
            ->get();
        $brand_by_id = DB::table('tbl_product')
            ->join('tbl_brand_product','tbl_brand_product.brand_id', '=', 'tbl_product.brand_id')
            ->where('tbl_product.brand_id', $brand_id)
            ->get();
        $brand_names = DB::table('tbl_brand_product')
            ->where('brand_id', $brand_id)
            ->limit(1)
            ->get();
        return view('papes.brand.show_brand')->with([
            'categoryProducts' => $categoryProducts,
            'brandProducts' => $brandProducts,
            'product_by_brands' => $brand_by_id,
            'brand_names' => $brand_names,
        ]);
    }
}
